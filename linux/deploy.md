# Deploy Django app + frontend

Nesse artigo iremos aprender como efetuar o deploy de uma aplicação django (backend) e um aplicação vuejs (como frontend) no servidor que preparamos.

## Clone

Para padronizar nossos scripts, vamos criar uma pasta projects na raiz do usuário `~` e ali dentro vamos colocar nossas aplicações.

```bash
cd ~
mkdir projects
```
