# Gitlab Auto Deploy - CI

Aqui, com nosso projeto já rodando, vamos implementar um CI (Continous Integration) para facilitar nossa vida.

Lembrando, que estou usando a stack:

* Hospedagem na AWS
* Ubuntu 18.04
* Pyenv para gerenciar as versões do python
* Pipenv para gerenciar os pacotes do projeto
* Uwsgi para manter a aplicação do backend funcionando
* Supervisor para manter o uwsgi funcionando

## Gitlab runner

Acesse seu projeto, vá no menu `Settings` > `CI / CD` > Runners e procure a parte do `Set up a specific Runner manually`

Ali encontraremos os 4 passos para fazer o runner funcionar.

### Instalação

Instale o runner  

```bash
sudo apt install gitlab-runner
```

> Mais detalhes em https://docs.gitlab.com/runner/install/

### Registro

Para registrar, vamos precisar de duas informações. A primeira, a url do runner e a segunda o token. Essas informações estão nos items 2 e 3 respectivamentes.

Entrão para efetuar o registro, rode o comando

```bash
sudo gitlab-runner register
```

Preenchimento

```bash
Running in system-mode.

Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
https://gitlab.com/
Please enter the gitlab-ci token for this runner:
** INSIRA O TOKEN AQUI **
Please enter the gitlab-ci description for this runner:
[runner]: ** DESCRIÇÃO DO RUNNER **
Please enter the gitlab-ci tags for this runner (comma separated):
** CASO QUEIRA APLICAR TAGS **
Whether to lock the Runner to current project [true/false]:
[true]: false
```

### Varieáveis

No caso, como os acessos as instancias da aws são atráves de uma chave (pem), precisamos coloca-la em forma de váriavel. Então acesse seu projeto na página do gitlab em `Settings` > `CI / CD` > Variables.

Ali vamos criar duas várieaveis para nosso CI. A primeira é o host do servidor `DEPLOY_SERVERS` que poderá ser vários (só separar por virgula). A segunda é a nossa chave de permissão (que veio da aws no arquivo .pem) `PRIVATE_KEY`.

### Scripts

Feito isso, vamos criar nossos scripts. Para organizá-los, vou criar uma pasta chamada `deploy`. Dentro dessa pasta vou organizar os scripts de forma que um possa ir chamando o outro, separando as responsabilidades de cada um.

Para disparar esses scripts, vamos criar um arquivo chamado `.gitlab-deploy.sh`

E finalmente, para fazer isso tudo funcionar, vamos implementar nosso `.gitlab-ci.yml`

```
├── deploy
│   ├── disableHostKeyChecking.sh
│   └── deploy.sh
├── .gitlab-ci.yml
├── .gitlab-deploy.sh
├── api
└── web
```

#### .gitlab-ci.yml

```yml
image: node:8.9.0

before_script:
  - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
  - chmod -R 775 ./deploy
stages:
  - deploy

deploy:dev:
  stage: deploy
  script:
    - bash .gitlab-deploy.sh
  only:
    - master
```

#### .gitlab-deploy.sh

```bash
#!/bin/bash

# any future command that fails will exit the script
set -e

# Lets write the public key of our aws instance
eval $(ssh-agent -s)
echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

# ** Alternative approach
# echo -e "$PRIVATE_KEY" > /root/.ssh/id_rsa
# chmod 600 /root/.ssh/id_rsa
# ** End of alternative approach

# disable the host key checking.
./deploy/disableHostKeyChecking.sh

# we have already setup the DEPLOYER_SERVER in our gitlab settings which is a
# comma seperated values of ip addresses.
DEPLOY_SERVERS=$DEPLOY_SERVERS

# lets split this string and convert this into array
# In UNIX, we can use this commond to do this
# ${string//substring/replacement}
# our substring is "," and we replace it with nothing.
ALL_SERVERS=(${DEPLOY_SERVERS//,/ })
echo "ALL_SERVERS ${ALL_SERVERS}"

# Lets iterate over this array and ssh into each EC2 instance
# Once inside the server, run updateAndRestart.sh
for server in "${ALL_SERVERS[@]}"
do
  echo "deploying to ${server}"
  ssh ubuntu@${server} 'bash' < ./deploy/deploy.sh
done
```

#### deploy/disableHostKeyChecking.sh

Para evitar erros do tipo confirme esse host, vamos aplicar o seguinte script

```bash
# This the the prompt we get whenever we ssh into the box and get the message like this
#
# The authenticity of the host 'ip address' cannot be verified....
#
# Below script will disable that prompt

# note ">>". It creates a file if it does not exits.
# The file content we want is below
#
# Host *
#   StrictHostKeyChecking no
#

# any future command that fails will exit the script
set -e
mkdir -p ~/.ssh
touch ~/.ssh/config
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config
```

#### deploy/deploy.sh

Finalmente quem vai fazer tudo

```bash
#!/bin/bash

set -e

# Change to deploy user
sudo su deploy
export PYENV_ROOT="/home/deploy/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
pyenv global 3.7.3
python -V

# pull
cd /home/deploy/projects/YOURPROJECT/
git reset --hard HEAD
git pull origin master --force

# Api process 
cd ./api/

pipenv run python manage.py makemigrations
pipenv run python manage.py migrate
pipenv run python manage.py collectstatic --noinput

# Back project dir
cd -

# Front end process
cd ./web/

yarn
yarn build --prod

# back to ubuntu user
exit

# go to root
sudo su
echo $(whoami)

# Restart supervisor
supervisorctl restart all
```
